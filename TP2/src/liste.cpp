#include "liste.hpp"


    Liste::Liste(){
        tete=nullptr;
    }

    Liste::~Liste(){
        for(Noeud* tmp=tete;tmp!=nullptr;tete=tmp){
            tmp=tmp->suivant;
            delete tete;
        }
    }

    void Liste::ajouterDevant(int val){
        Noeud* n=new Noeud();
        n->valeur =val;
        n->suivant=tete;
        tete=n;
    }

    int Liste::getTaille(){
        Noeud* courant=tete;
        int a=0;
        if(tete==nullptr){
            return a;
        }
        while(courant){
            a++;
            courant=courant->suivant;
        }
        return a;
    }

    int Liste::getElement(int indice){
        Noeud* courant=tete;
        for(int i=0;i<indice;i++){
            courant=courant->suivant;
        }

        return courant->valeur;
    }
