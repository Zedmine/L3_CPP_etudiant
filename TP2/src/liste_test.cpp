#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe){};

TEST(GroupListe,Liste_test1){
    Liste l;
    CHECK(l.tete==nullptr);
}

TEST(GroupListe,Liste_test2){
    Liste l1;
    l1.ajouterDevant(5);
    CHECK(l1.tete->valeur==5);
}

TEST(GroupListe,Liste_test3){
    Liste l2;
    l2.ajouterDevant(4);
    l2.ajouterDevant(7);
    int i=l2.getTaille();
    CHECK(i==2);
}

TEST(GroupListe,Liste_test4){
    Liste l3;
    l3.ajouterDevant(4);
    l3.ajouterDevant(5);
    l3.ajouterDevant(6);
    int a=l3.getElement(1);
    CHECK(a==5);
}
