#include "Doubler.hpp"
#include "liste.hpp"
#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;

    //1
    int a=42;
    int *p =&a;
    *p=37;
    std::cout<<" Question 1:" <<p<<";"<<a<<std::endl;

    //2
    int *t=new int[10];
    t[2]=42;
    std::cout<<"Question 2a:"<<t[2]<<std::endl;
    delete [] t;
    t=nullptr;
    std::cout<<"Question 2b:"<<t<<std::endl;

    //3
    Liste l;
    l.ajouterDevant(13);
    l.ajouterDevant(37);
    Noeud* courant=l.tete;
    for(int i=0; i<l.getTaille();i++){
        std::cout<<courant->valeur<<std::endl;
        courant=courant->suivant;
    }

    return 0;
}

