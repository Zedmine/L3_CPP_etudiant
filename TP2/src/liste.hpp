#ifndef LISTE_HPP
#define LISTE_HPP

struct Noeud{
    int valeur;
    Noeud* suivant;
};

struct Liste{
    Noeud* tete;

    //Constructeur
    Liste();

    //Destructeur
    ~Liste();

    //Méthodes
    void ajouterDevant(int val);

    int getTaille();

    int getElement(int indice);
};
#endif // LISTE_HPP
